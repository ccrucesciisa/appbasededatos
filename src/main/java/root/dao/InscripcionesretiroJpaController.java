/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.entity.Inscripcionesretiro;

/**
 *
 * @author ccruces
 */
public class InscripcionesretiroJpaController implements Serializable {

    public InscripcionesretiroJpaController() {
    
    }
    
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit"); 

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inscripcionesretiro inscripcionesretiro) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(inscripcionesretiro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInscripcionesretiro(inscripcionesretiro.getRut()) != null) {
                throw new PreexistingEntityException("Inscripcionesretiro " + inscripcionesretiro + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inscripcionesretiro inscripcionesretiro) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            inscripcionesretiro = em.merge(inscripcionesretiro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = inscripcionesretiro.getRut();
                if (findInscripcionesretiro(id) == null) {
                    throw new NonexistentEntityException("The inscripcionesretiro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscripcionesretiro inscripcionesretiro;
            try {
                inscripcionesretiro = em.getReference(Inscripcionesretiro.class, id);
                inscripcionesretiro.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inscripcionesretiro with id " + id + " no longer exists.", enfe);
            }
            em.remove(inscripcionesretiro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inscripcionesretiro> findInscripcionesretiroEntities() {
        return findInscripcionesretiroEntities(true, -1, -1);
    }

    public List<Inscripcionesretiro> findInscripcionesretiroEntities(int maxResults, int firstResult) {
        return findInscripcionesretiroEntities(false, maxResults, firstResult);
    }

    private List<Inscripcionesretiro> findInscripcionesretiroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inscripcionesretiro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inscripcionesretiro findInscripcionesretiro(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inscripcionesretiro.class, id);
        } finally {
            em.close();
        }
    }

    public int getInscripcionesretiroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inscripcionesretiro> rt = cq.from(Inscripcionesretiro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
